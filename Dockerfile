FROM openjdk:8-jdk-alpine
# RUN touch /app.jar

COPY target/spring-petclinic-2.5.0-SNAPSHOT.jar /spring-petclinic.jar
COPY target/classes/static /static

EXPOSE 8080

ENTRYPOINT ["java","-jar","spring-petclinic.jar"]
